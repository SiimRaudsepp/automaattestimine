package fileInputTest;

import input.fileInput.FileInput;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class FileInputTest {
    private FileInput mockedFileInput;
    private FileInput fileInput;

    @Before
    public void testSetUp() {
        mockedFileInput = mock(FileInput.class);
        fileInput = new FileInput("src/test/java/fileInputTest/testInputs");
    }

    @Test
    public void testWritingFirstLineFromFile() {
        String jsonData = fileInput.readInputFromFile()[0];
        mockedFileInput.saveInputToFile(jsonData);
        verify(mockedFileInput).saveInputToFile("{'city':'Lahti', 'countryCode':'fin'}");
    }

    @Test
    public void testWritingLastLineFromFile() {
        String jsonData = fileInput.readInputFromFile()[2];
        mockedFileInput.saveInputToFile(jsonData);
        verify(mockedFileInput).saveInputToFile("{'city':'Washington', 'countryCode':'us'}");
    }

    @Test
    public void testReadingAllInputs() {
        String[] inputsFromFile = fileInput.readInputFromFile();
        String[] wantedInputs = {
                "{'city':'Lahti', 'countryCode':'fin'}",
                "{'city':'Brighton', 'countryCode':'gb'}",
                "{'city':'Washington', 'countryCode':'us'}"
        };
        assertTrue(Arrays.equals(inputsFromFile, wantedInputs));
    }

    @Test
    public void testSavingOutPutToFile() {
        fileInput.saveInputToFile("{'city':'Moscow', 'countryCode':'ru'}");
        File folder = new File("src/main/java/weatherRepo/weatherReportByCity");
        List<String> listOfFiles = Arrays.asList(folder.list());
        assertTrue(listOfFiles.contains("Moscow.txt"));
        try {
            Files.delete(Paths.get("src/main/java/weatherRepo/weatherReportByCity/Moscow.txt"));
        } catch (IOException e) {
            System.out.println("No such file!");
        }
    }

}
