package getWeatherDataTest;

import org.junit.Before;
import org.junit.Test;
import parseJson.ParseJson;
import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetWeatherDataTest {
    private WeatherRepo mockedRepo;
    private ParseJson mockedParser;
    private GetWeatherData getter;

    @Before
    public void testSetUp() {
        mockedRepo = mock(WeatherRepo.class);
        mockedParser = mock(ParseJson.class);
    }

    @Test
    public void testGettingWeatherData() {
        when(mockedParser.getDataFromJson("wind", mockedRepo.weatherJson)).thenReturn("strong");
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals("strong", getter.getWeatherData("wind"));
    }

    @Test
    public void testGettingForecastData() {
        when(mockedParser.getDataFromJson("rain", mockedRepo.forecastJson)).thenReturn("yes");
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals("yes", getter.getWeatherData("rain"));
    }

    @Test
    public void testForecastDayExtremaTemperatures() {
        String temps = "1 5 -1 0 11 30";
        String dates = "2017-12-19 2017-12-19 2017-12-20 2017-12-20 2017-12-21 2017-12-21";
        when(mockedParser.getDataFromJson("temp_max", mockedRepo.forecastJson)).thenReturn(temps);
        when(mockedParser.getDataFromJson("dt_txt", mockedRepo.forecastJson)).thenReturn(dates);
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals("-1", getter.getForecastDayExtremaTemperatures(1, "max").get(0));
    }

    @Test
    public void testGettingForecastDate() {
        String dates = "2017-12-18 2017-12-18 2017-12-19 2017-12-19 2017-12-20 2017-12-20 2017-12-21 2017-12-21";
        when(mockedParser.getDataFromJson("dt_txt", mockedRepo.forecastJson)).thenReturn(dates);
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals("2017-12-20", getter.getForecastDate(2));
    }

    @Test
    public void testGettingXCord() {
        Double xCord = 44.00;
        when(mockedParser.getDataFromJson("lat", mockedRepo.forecastJson)).thenReturn(xCord.toString());
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals(44.00, getter.getCords("x"), 0.0001);
    }
    @Test
    public void testGettingYCord() {
        Double xCord = 22.00;
        when(mockedParser.getDataFromJson("lon", mockedRepo.forecastJson)).thenReturn(xCord.toString());
        getter = new GetWeatherData(mockedRepo, mockedParser);
        assertEquals(22.00, getter.getCords("y"), 0.0001);
    }
}
