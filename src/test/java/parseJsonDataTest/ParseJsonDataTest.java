package parseJsonDataTest;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import parseJson.ParseJson;

import static org.junit.Assert.assertEquals;

public class ParseJsonDataTest {
    private ParseJson parser;
    private JSONObject json;
    @Before
    public void setUp() {
        parser = new ParseJson();
    }

    @Test
    public void testGettingDataFromJson() {
        json = new JSONObject("{'name':'Siim', 'age':'20'}");
        String soughtName = parser.getDataFromJson("name", json);
        assertEquals("Siim", soughtName);
    }
    @Test
    public void testGettingDataFromJsonArray() {
        json = new JSONObject("{'name':'Siim', 'age':'20', 'sports':[{'football':'yes', 'basketball':'no'}]}");
        String soughtData = parser.getDataFromJson("football", json);
        assertEquals("yes", soughtData);
    }
    @Test
    public void testGettingDataFromJsonArrayThatIsInsideAnArray() {
        json = new JSONObject("{'name':'Siim', 'age':'20', 'films':[{'good':[{'2017':'Nemo 2', '2010':'Nemo'}], 'bad':'no'}]}");
        String soughtData = parser.getDataFromJson("2010", json);
        assertEquals("Nemo", soughtData);
    }
}
