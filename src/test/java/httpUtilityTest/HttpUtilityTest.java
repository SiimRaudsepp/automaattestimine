package httpUtilityTest;

import org.junit.Before;
import org.junit.Test;
import weatherRepo.WeatherRepo;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class HttpUtilityTest {
    private WeatherRepo repo;

    @Before
    public void setUp() {
        repo = new WeatherRepo("Tallinn", "ee");
    }
    @Test

    public void testHttpAddressWeatherFeature() {
        assertTrue(repo.weatherUrl.contains("weather"));
    }
    @Test
    public void testHttpAddressForecastFeature() {
        assertTrue(repo.forecastUrl.contains("forecast"));
    }
    @Test
    public void testHttpAddressContainsKey() {
        assertTrue(repo.forecastUrl.contains("c7de43b3c540a76625ce82246ae306d1"));
    }
    @Test
    public void testHttpAddressRegion() {
        assertTrue(repo.forecastUrl.contains("Tallinn,ee"));
    }
    @Test
    public void testHttpConnection(){
        try {
            URL url = new URL(repo.forecastUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            assertEquals(200, con.getResponseCode());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testHttpResponse(){
        try {
            URL url = new URL(repo.forecastUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            assertEquals("OK", con.getResponseMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void testHttpRequest(){
        try {
            URL url = new URL(repo.forecastUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            assertEquals("GET", con.getRequestMethod());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
