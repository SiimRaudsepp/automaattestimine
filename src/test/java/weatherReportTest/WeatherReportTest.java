package weatherReportTest;

import org.junit.Before;
import org.junit.Test;
import parseJson.ParseJson;
import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;
import weatherReport.Report;
import java.time.LocalDate;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class WeatherReportTest {
    private Double currentDayTemp;
    private Double forecastTempFirst;
    private Double forecastTempSecond;
    private Double forecastTempThird;
    private Double xCordFromApi;
    private Double yCordFromApi;
    private String currentDate;
    private String thirdDateFromApi;
    private double forecastTempFirstMax;
    private double forecastTempSecondMax;
    private double forecastTempThirdMax;
    private String report;
    private GetWeatherData dataWeather;

    @Before
    public void setup() {
        WeatherRepo repo = new WeatherRepo("Tallinn", "ee");
        dataWeather = new GetWeatherData(repo, new ParseJson());
        Report report = new Report("Tallinn", "ee", dataWeather);
        currentDayTemp = Double.parseDouble(report.currentWeatherTemp());
        forecastTempFirst = Double.parseDouble(report.forecastWeatherTemperatureExtrema(1, "min"));
        forecastTempSecond = Double.parseDouble(report.forecastWeatherTemperatureExtrema(2, "min"));
        forecastTempThird = Double.parseDouble(report.forecastWeatherTemperatureExtrema(3, "min"));
        forecastTempFirstMax = Double.parseDouble(report.forecastWeatherTemperatureExtrema(1, "max"));
        forecastTempSecondMax = Double.parseDouble(report.forecastWeatherTemperatureExtrema(2, "max"));
        forecastTempThirdMax = Double.parseDouble(report.forecastWeatherTemperatureExtrema(3, "max"));
        xCordFromApi = dataWeather.getCords("x");
        yCordFromApi = dataWeather.getCords("y");
        currentDate = dataWeather.getForecastDate(0);
        thirdDateFromApi = dataWeather.getForecastDate(3);
        this.report = report.weatherReport();
    }

    @Test
    public void testCurrentTemperatureNotNull() {
        assertTrue(currentDayTemp != null);
    }

    @Test
    public void testCurrentTemperatureNotExtreme() {
        assertTrue(-60 < currentDayTemp && currentDayTemp < 60);
    }

    @Test
    public void testForecastFirstDayTempNotExtreme() {
        assertTrue(-60 < forecastTempFirst && forecastTempFirst < 60);
    }

    @Test
    public void testForecastFirstDayTempNotNull() {
        assertTrue(forecastTempFirst != null);
    }

    @Test
    public void testForecastSecondDayTempNotExtreme() {
        assertTrue(-60 < forecastTempSecond && forecastTempSecond < 60);
    }

    @Test
    public void testForecastSecondDayTempNotNull() {
        assertTrue(forecastTempSecond != null);
    }

    @Test
    public void testForecastThirdDayTempNotExtreme() {
        assertTrue(-60 < forecastTempThird && forecastTempThird < 60);
    }

    @Test
    public void testForecastThirdDayTempNotNull() {
        assertTrue(forecastTempThird != null);
    }

    @Test
    public void testFirstDayMinAndMax() {
        assertTrue(forecastTempFirst <= forecastTempFirstMax);
    }

    @Test
    public void testSecondDayMinAndMax() {
        assertTrue(forecastTempSecond <= forecastTempSecondMax);
    }

    @Test
    public void testThirdDayMinAndMax() {
        assertTrue(forecastTempThird <= forecastTempThirdMax);
    }

    @Test
    public void testXCordWithServerLocation() {
        double serverXCord = 59;
        assertTrue(serverXCord < xCordFromApi && serverXCord + 1 > xCordFromApi);
    }

    @Test
    public void testYCordWithServerLocation() {
        double serverYCord = 24;
        assertTrue(serverYCord < yCordFromApi && serverYCord + 1 > yCordFromApi);
    }

    @Test
    public void testYCordNotNull() {
        assertTrue(yCordFromApi != null);
    }

    @Test
    public void testXCordNotNull() {
        assertTrue(xCordFromApi != null);
    }

    @Test
    public void testCurrentDate() {
        String dateCurrent = LocalDate.now().toString();
        assertTrue(dateCurrent.equals(currentDate));
    }

    @Test
    public void testThirdDayDate() {
        String dateCurrent = LocalDate.now().plusDays(3).toString();
        assertTrue(dateCurrent.equals(thirdDateFromApi));
    }

    @Test
    public void testReportContainsCurrentTemp() {
        Report reportCurrent = new Report("Tallinn", "ee", dataWeather);
        assertTrue(report.contains(reportCurrent.currentWeatherTemp()));
    }

    @Test
    public void testReportContainsTomorrowsDate() {
        String dateTomorrow = LocalDate.now().plusDays(1).toString();
        assertTrue(report.contains(dateTomorrow));
    }

    @Test
    public void testCurrentTempHasTwoDigitsAfterComa() {
        Report reportCurrent = new Report("Tallinn", "ee", dataWeather);
        String currentTemp = reportCurrent.currentWeatherTemp();
        int afterComa = currentTemp.substring(currentTemp.indexOf(".") + 1, currentTemp.length()).length();
        System.out.println(currentTemp);
        assertEquals(2, afterComa);
    }
}
