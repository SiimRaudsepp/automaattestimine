package weatherReportTest;

import org.junit.Before;
import org.junit.Test;
import parseWeatherData.GetWeatherData;
import weatherReport.Report;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MockedReportTests {
    private Report report;
    private GetWeatherData mockedGetter;

    @Before
    public void setUp() {
        mockedGetter = mock(GetWeatherData.class);
    }

    @Test
    public void testCurrentTemp() {
        when(mockedGetter.getWeatherData("temp")).thenReturn("0.0");
        report = new Report("Tallinn", "ee", mockedGetter);
        assertEquals(-273.15, Double.parseDouble(report.currentWeatherTemp()), 0.0001);
    }

    @Test
    public void testForecastWeatherTemperatureExtrema() {
        when(mockedGetter.getForecastDayExtremaTemperatures(1, "min"))
                .thenReturn(Arrays.asList("1", "0", "9"));
        report = new Report("Tallinn", "ee", mockedGetter);
        assertEquals(-273.15,
                Double.parseDouble(report.forecastWeatherTemperatureExtrema(1, "min")),
                0.0001);
    }

    @Test
    public void testReport() {
        String expectedReport =
                "Brighton\r\n" +
                "\r\n" +
                "Coordinates: 50.82 : -0.14\r\n" +
                "Current temperature: 9.40 °C\r\n" +
                "2017-12-19: -2.74 to 8.17 °C\r\n" +
                "2017-12-20: 7.40 to 10.62 °C\r\n" +
                "2017-12-21: 8.20 to 11.05 °C\r\n";
        when(mockedGetter.getCords("x")).thenReturn(50.82);
        when(mockedGetter.getCords("y")).thenReturn(-0.14);
        when(mockedGetter.getForecastDate(1)).thenReturn("2017-12-19");
        when(mockedGetter.getForecastDate(2)).thenReturn("2017-12-20");
        when(mockedGetter.getForecastDate(3)).thenReturn("2017-12-21");
        when(mockedGetter.getWeatherData("temp")).thenReturn("282.55");
        when(mockedGetter.getForecastDayExtremaTemperatures(1, "min"))
                .thenReturn(Collections.singletonList("270.41"));
        when(mockedGetter.getForecastDayExtremaTemperatures(1, "max"))
                .thenReturn(Collections.singletonList("281.32"));
        when(mockedGetter.getForecastDayExtremaTemperatures(2, "min"))
                .thenReturn(Collections.singletonList("280.55"));
        when(mockedGetter.getForecastDayExtremaTemperatures(2, "max"))
                .thenReturn(Collections.singletonList("283.77"));
        when(mockedGetter.getForecastDayExtremaTemperatures(3, "min"))
                .thenReturn(Collections.singletonList("281.35"));
        when(mockedGetter.getForecastDayExtremaTemperatures(3, "max"))
                .thenReturn(Collections.singletonList("284.2"));
        report = new Report("Brighton", "uk", mockedGetter);
        assertEquals(expectedReport, report.weatherReport());
    }
}
