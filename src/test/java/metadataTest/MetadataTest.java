package metadataTest;

import org.junit.Test;
import weatherRepo.WeatherRepo;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import static org.junit.Assert.assertEquals;

public class MetadataTest {
    @Test
    public void testFileExtension() throws IOException {
        WeatherRepo repo = new WeatherRepo("Tallinn", "ee");
        URLConnection conn = new URL(repo.weatherUrl).openConnection();
        String type = conn.getContentType();
        assertEquals(true, type.contains("json"));
    }
}
