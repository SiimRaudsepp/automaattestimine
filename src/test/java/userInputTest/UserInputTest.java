package userInputTest;

import input.userInput.UserInput;
import org.junit.Before;
import org.junit.Test;
import parseJson.ParseJson;
import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;
import weatherReport.Report;
import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.Assert.assertEquals;

public class UserInputTest {
    private UserInput userInput;
    private GetWeatherData getter = new GetWeatherData(
            new WeatherRepo("Tallinn", "ee"),
            new ParseJson()
    );

    @Before
    public void setUp() {
        userInput = new UserInput();
    }

    @Test
    public void manualInputCityName() throws Exception {
        String expectedReport = new Report("Tallinn", "ee", getter).weatherReport();
        InputStream is = System.in;
        System.setIn(new ByteArrayInputStream("Tallinn ee".getBytes()));
        String reportFromSysIn = userInput.getInputAndMakeReport();
        System.setIn(is);
        assertEquals(expectedReport, reportFromSysIn);
    }

}
