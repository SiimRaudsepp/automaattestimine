package input.fileInput;

import org.json.JSONObject;
import parseJson.ParseJson;
import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;
import weatherReport.Report;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileInput {
    public FileInput() {

    }
    public FileInput(String filePath) {
        inputPath = filePath;
    }

    private String inputPath = "src/main/java/weatherRepo/input.txt";

    public void readFromInputAndSaveToOutput() {
        String[] array = readInputFromFile();
        for (String dataFromArray : array) {
            saveInputToFile(dataFromArray);
        }
    }

    public String[] readInputFromFile() {
        String[] array;
        try (FileReader file = new FileReader(inputPath)) {
            BufferedReader reader = new BufferedReader(file);
            String line;
            List<String> list = new ArrayList<>();
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
            array = new String[list.size()];
            file.close();
            reader.close();
            return list.toArray(array);
        } catch (IOException e) {
            System.out.println("Could not read file!");
        }
        return null;
    }

    public void saveInputToFile(String stringDataInJson) {
        String city = new JSONObject(stringDataInJson).getString("city");
        String countryCode = new JSONObject(stringDataInJson).getString("countryCode");
        GetWeatherData getter = new GetWeatherData(new WeatherRepo(city, countryCode), new ParseJson());
        Report report = new Report(city, countryCode, getter);
        try (FileWriter file = new FileWriter("src/main/java/weatherRepo/weatherReportByCity/" + city + ".txt")) {
            BufferedWriter writer = new BufferedWriter(file);
            writer.append(report.weatherReport());
            writer.newLine();
            writer.close();
            file.close();
        } catch (IOException e) {
            System.out.println("Could not save data to file!");
        }
    }
}
