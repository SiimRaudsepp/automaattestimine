package input.userInput;

import parseJson.ParseJson;
import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;
import weatherReport.Report;
import java.util.Scanner;

public class UserInput {
    private String getCityAndCountryCode() {
        System.out.println("Enter City and country code: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public String getInputAndMakeReport() {
        String[] input = getCityAndCountryCode().split(" ");
        Report report = new Report(
                input[0],
                input[1],
                new GetWeatherData(new WeatherRepo(input[0], input[1]), new ParseJson())
        );
        return report.weatherReport();
    }
}
