package weatherReport;

import parseWeatherData.GetWeatherData;
import weatherRepo.WeatherRepo;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class Report {
    private WeatherRepo repo;
    private final double ABS_MIN_TEMP = 273.15;
    private GetWeatherData getter;

    public Report(String city, String countryCode, GetWeatherData getter) {
        repo = new WeatherRepo(city, countryCode);
        this.getter = getter;
    }

    public String currentWeatherTemp() {
        return String.format(Locale.ROOT, "%.2f", Double.parseDouble(getter.getWeatherData("temp"))- ABS_MIN_TEMP);
    }

    public String forecastWeatherTemperatureExtrema(int dayFromNow, String temperatureExtrema) {
        List<String> tempValues = getter.getForecastDayExtremaTemperatures(dayFromNow, temperatureExtrema);
        if (Objects.equals(temperatureExtrema, "min")) {
            return String.format(Locale.ROOT, "%.2f", Double.parseDouble(Collections.min(tempValues)) - ABS_MIN_TEMP);
        }
        return String.format(Locale.ROOT, "%.2f", Double.parseDouble(Collections.max(tempValues)) - ABS_MIN_TEMP);
    }

    public String weatherReport() {
        String br = "\r\n";
        return repo.city + br + br
                + "Coordinates: " + getter.getCords("x") + " : " + getter.getCords("y") + br
                + "Current temperature: " + currentWeatherTemp() + " °C" + br
                + getter.getForecastDate(1) + ": "
                + forecastWeatherTemperatureExtrema(1, "min")
                + " to " + forecastWeatherTemperatureExtrema(1, "max") + " °C" + br
                + getter.getForecastDate(2) + ": "
                + forecastWeatherTemperatureExtrema(2, "min")
                + " to " + forecastWeatherTemperatureExtrema(2, "max") + " °C" + br
                + getter.getForecastDate(3) + ": "
                + forecastWeatherTemperatureExtrema(3, "min")
                + " to " + forecastWeatherTemperatureExtrema(3, "max") + " °C" + br;
    }
}
