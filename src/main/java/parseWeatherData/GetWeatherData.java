package parseWeatherData;

import parseJson.ParseJson;
import weatherRepo.WeatherRepo;

import java.time.LocalDate;
import java.util.*;

public class GetWeatherData {
    private WeatherRepo repo;
    private ParseJson parseJson;

    public GetWeatherData(WeatherRepo repo, ParseJson parser) {
        this.repo = repo;
        parseJson = parser;
    }

    public String getWeatherData(String data) {
        return parseJson.getDataFromJson(data, repo.weatherJson);
    }

    public String getForecastData(String data) {
        return parseJson.getDataFromJson(data, repo.forecastJson);
    }

    public List<String> getForecastDayExtremaTemperatures(int daysFromNow, String extrema) {
        String[] arrayMax = getForecastData("temp_" + extrema).split(" ");
        String[] arrayDate = getForecastData("dt_txt").split(" ");
        List<String> tempValues = new ArrayList<>();
        LocalDate date = LocalDate.now().plusDays(daysFromNow);
        int maxTempIndex = 0;
        for (int dateFromArray = 0; dateFromArray < arrayDate.length; dateFromArray += 2) {
            if (isGivenDateSoughtDate(arrayDate[dateFromArray], date.toString())) {
                tempValues.add(arrayMax[maxTempIndex]);
            }
            maxTempIndex++;
        }
        return tempValues;
    }

    private boolean isGivenDateSoughtDate(String givenDate, String soughtDate) {
        return Objects.equals(givenDate, soughtDate);
    }

    public String getForecastDate(int dayFromNow) {
        if (dayFromNow <= 5) {
            String[] dates = getForecastData("dt_txt").split(" ");
            List<String> listOfDates = new ArrayList<>();
            for (int forecastDate = 0; forecastDate < dates.length; forecastDate += 2) {
                listOfDates.add(dates[forecastDate]);
            }
            Set<String> removeDuplicatesFromList = new LinkedHashSet<>(listOfDates);
            listOfDates.clear();
            listOfDates.addAll(removeDuplicatesFromList);
            return listOfDates.get(dayFromNow);
        }
        return null;
    }

    public double getCords(String coordinateType) {
        if (Objects.equals(coordinateType, "y")){
            return Double.parseDouble(getWeatherData("lon"));
        } else if (Objects.equals(coordinateType, "x")) {
            return Double.parseDouble(getWeatherData("lat"));
        }
        System.out.println("Wrong cords!");
        return 0;
    }
}
