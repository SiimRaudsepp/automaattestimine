package parseJson;

import org.json.JSONArray;
import org.json.JSONObject;

public class ParseJson {

    public String getDataFromJson(String dataKey, JSONObject json) {
        StringBuilder answer = new StringBuilder();
        if (json.keySet().contains(dataKey)) {
            return json.get(dataKey).toString();
        }
        for (String jsonKey : json.keySet()) {
            if (isJsonObject(json, jsonKey)) {
                answer.append(" ");
                answer.append(this.getDataFromJson(dataKey, json.getJSONObject(jsonKey)));
            } else if (isJsonArray(json, jsonKey)) {
                JSONArray array = (JSONArray) json.get(jsonKey);
                for (int key = 0; key < array.length(); key++) {
                    answer.append(" ");
                    answer.append(this.getDataFromJson(dataKey, array.getJSONObject(key)));
                }
            }
        }
        answer.append(" ");
        return answer.toString().trim();
    }

    private boolean isJsonArray(JSONObject json, String jsonKey) {
        return json.get(jsonKey).getClass().equals(JSONArray.class);
    }
    private boolean isJsonObject(JSONObject json, String jsonKey) {
        return json.getClass().equals(json.get(jsonKey).getClass()) && json.getJSONObject(jsonKey).keySet().size() != 0;
    }
}
