package readJson;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class JsonReader {
    private String url;

    public JsonReader(String url) {
        this.url = url;
    }

    public JSONObject getJsonFromUrl() {
        try (InputStreamReader streamReader =
                     new InputStreamReader((new URL(url)).openConnection().getInputStream(), Charset.forName("UTF-8"))) {
            BufferedReader reader = new BufferedReader(streamReader);
            String line;
            StringBuilder jsonText = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                jsonText.append(line);
            }
            return new JSONObject(jsonText.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Could not read json!");
        return null;
    }
}
