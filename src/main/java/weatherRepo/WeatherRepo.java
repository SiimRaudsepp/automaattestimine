package weatherRepo;

import org.json.JSONObject;
import readJson.JsonReader;

public class WeatherRepo {
    public String city;
    public String countryCode;
    public String weatherUrl;
    public String forecastUrl;
    public JSONObject weatherJson;
    public JSONObject forecastJson;

    public WeatherRepo (String city, String countryCode) {
        this.city = city;
        this.countryCode = countryCode;
        weatherUrl = "http://api.openweathermap.org/data/2.5/weather?q="
                + city
                + ","
                + countryCode
                + "&APPID=c7de43b3c540a76625ce82246ae306d1";
        forecastUrl = "http://api.openweathermap.org/data/2.5/forecast?q="
                + city
                + ","
                + countryCode
                + "&APPID=c7de43b3c540a76625ce82246ae306d1";
        weatherJson = new JsonReader(weatherUrl).getJsonFromUrl();
        forecastJson = new JsonReader(forecastUrl).getJsonFromUrl();
    }
}
